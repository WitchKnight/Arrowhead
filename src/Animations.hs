{-# LANGUAGE FlexibleInstances,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Animations where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS

import Data.HashMap.Strict((!))
import Data.Hashable

import Data.IORef

import Lib
import Conf
import View


data Animation = Animation {
  _animSize   :: V2 Int,
  _animOffset :: V2 Float,
  _animText   :: Texture,
  _animDurs   :: [Int],
  _animMov    :: [V2 Float],
  _animRun    :: Bool,
  _animFlip   :: IORef (V2 Bool),
  _curSprite  :: Sprite,
  _animTime   :: IORef Time 
}

instance Show Animation where
  show (Animation size offset _ durs mov run _ _ _) = 
    (show size) ++ (show offset) ++ (show durs) ++ (show mov) ++ (show run)

makeLenses ''Animation


initSizedAnim texture info mov size offset= do
  anim <- initAnim texture info mov
  return $ anim & animSize .~ size 
                & animOffset .~ offset


initAnim' texture info  = initAnim texture info []
initAnim texture info mov= do
  emv <- newEmptyMVar
  spr <- err $ createSprite
  tm <- newIORef timeZero
  flip <- newIORef $ V2 False False
  anim <- return $ Animation {
      _animSize = V2 tSZ tSZ,
      _animOffset = V2 0 0,
      _animText = texture,
      _animDurs = info,
      _animMov = mov,
      _animRun = True,
      _animFlip = flip,
      _curSprite = spr,
      _animTime = tm
    }
  setTexture (anim^.curSprite) (anim^.animText) True

  updateAnim 0 anim   
  return anim

setAnimTime anim = writeIORef (anim^.animTime)

resetAnim anim = do
  setAnimTime anim 0


flipv anim =  do
  modifyIORef (anim^.animFlip) flipy
fliph anim =  do
  modifyIORef (anim^.animFlip) flipx

flipved anim = do
  let anim2 = anim
  flipv anim2
  return anim2


fliphed anim = do
  let anim2 = anim
  fliph anim2
  return anim2

setAnimDir d anim = do
  state <- readIORef $ anim^.animFlip
  let flipstate | d == 4 = V2 True False
                | d == 5 = state
                | d == 6 = V2 False False
  writeIORef (anim^.animFlip) flipstate

withAnimDir d anim = do
  setAnimDir d anim 
  return anim

getCurrent anim = case anim^.animDurs of
    [] -> return $ anim^.curSprite
    durations -> do
      newTime <- readIORef $ anim^.animTime
      let framesPassed =(60*asMilliseconds newTime) `div` (1000)
      -- we tie in the loop to the total number of frames of the animation
      let currentFrame = framesPassed `rem` (sum durations)
      let index = findIndex currentFrame durations
      Vec2u imgw' imgh <- textureSize (anim^.animText)
      let V2 sx sy = anim^.animSize
      let imgw =fromIntegral $  div imgw' $ fromIntegral  sx
      let row = index `div` imgw
      let col = index `rem` imgw
      setTextureRect (anim^.curSprite)  $ IntRect (sx*col) (sy*row) sx sy
      -- centerSprite $ anim^.curSprite
      return $ anim^.curSprite

updateAnim :: Time  -> Animation -> IO ()
updateAnim time anim = case anim^.animDurs of
    [] -> return ()
    durations -> do
      modifyIORef (anim^.animTime) (time+)
      return ()

getIndex durations time = index 
      where
        framesPassed =(60*asMilliseconds time) `div` (1000)
      -- we tie in the loop to the total number of frames of the animation
        currentFrame = framesPassed `rem` (sum durations)
        index = findIndex currentFrame durations


getMov anim = case anim^.animDurs of
  [] -> return (V2 0 0)
  durations -> case anim^.animMov of
    [] -> return (V2 0 0)
    moves -> do
      newTime <- readIORef $ anim^.animTime
      let index = getIndex durations newTime
      return $ moves !! index

findIndex frames durations = findIndex' frames 0 durations 0
findIndex' frames i [] accum = 0      
findIndex' frames i (d:ds) accum
  | accum + d >= frames = i
  | otherwise = findIndex' frames (i+1) ds (d+accum)

cleanAnim anim = do
  destroy (anim^.curSprite)
  destroy (anim^.animText)

instance Viewable Animation where
  drawWithTransform tg transf anim = do
    spr <- getCurrent anim
    (V2 x y) <- getMov anim
    V2 w h <- readIORef $ anim^.animFlip
    let V2 ox oy = anim^.animOffset 
    let V2 sx sy = anim^.animSize
    let V2 x' y' = V2 (if w then x + (fromIntegral sx)/ 2 else x) (if h then y + (fromIntegral  sy)/2 else y)
    let transl = translation (ox + negOrPos w x) (oy + negOrPos h y) 
    centerSprite spr
    drawSprite tg spr $ Just $ renderStates  { transform= transf * transl *  (scaling (if w then -1 else 1) (if h then -1 else 1) ) }

instance Anim Animation where
  drawWithTimeAndTransform tg tm transf anim = do
    setAnimTime anim tm
    drawWithTransform tg transf anim
{-# LANGUAGE RankNTypes,OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Game where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable

import Data.IORef

import Lib
import Conf
import View
import GameText
import GameData
import Animations

data Action game =  GameAction (game -> IO game) | OutsideAction Int



class Scene a where
  getTimeRef          :: a -> IORef Time
  getTime             :: a -> IO Time
  getTime s = do
    t <- readIORef  $ getTimeRef s
    return t
  setTime             :: Time -> a -> IO ()
  setTime tm bS = writeIORef (getTimeRef bS) tm
  addTime             :: Time -> a -> IO ()
  addTime dt s = do
    let t = getTimeRef s
    modifyIORef t (+dt)
  drawWithEnv         :: SFRenderTarget b  => b -> GameEnv -> a -> IO ()
  dWE                 :: SFRenderTarget b  => b -> GameEnv -> a -> IO ()
  dWE = drawWithEnv
  updateScene         :: Time -> a -> IO a 
  updateScene dt s = do
    addTime dt s
    return s
  
  
class Scene b => GameState a b | a -> b where
  getOCode  :: a -> Maybe Int
  withOCode :: a -> Int -> a
  noCode  :: a -> a
  envG    :: a -> GameEnv
  scenG   :: a  -> b
  withSc  :: b -> a -> a
  wndG    :: a -> RenderWindow 
  updateGame :: Time -> a -> IO a
  updateGame dt g = do
    nS <- updateScene dt (scenG g)
    return $ withSc nS g 
  drawG :: SFRenderTarget c  => c ->  a -> IO ()
  drawG tg g = do
    dWE tg (envG g) (scenG g)
  drawGame :: a -> IO ()
  drawGame g = do
    let wnd = wndG g
    clearRenderWindow wnd black
    drawG wnd g
    display wnd
  cleanGame :: a -> IO ()
  cleanGame g =do
    cleanEnv (envG g)
    destroy (wndG g)
  mapAction  ::  a -> Action a-> IO a
  mapAction g (GameAction f) = f g
  mapAction g (OutsideAction i) = return (withOCode g i)
  execAction ::a -> Action a-> IO ()
  execAction g (GameAction f) = mapAction g (GameAction f) >> return ()

-- instance Anim TestG where
--   drawWithTimeAndTransform tg time transf  (G menu env) = do
--     print "im drawin lol"
--     HIO.mapM_ f (menu^.mElems)
--       where
--       f (crd,el) =  do
--         let V2 x y = getPos crd 
--         let transf =  (translation x y)
--         print (x,y)
--         (el^.uiLabel) >?> \x -> do
--           t <- getText (x^.lId) (env^.gameText)
--           t >?> drawWithTransform tg transf
--         dwtat tg time transf el

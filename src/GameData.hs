{-# LANGUAGE OverloadedStrings,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}

module GameData where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable

import Data.IORef

import Lib
import Conf
import View
import GameText
import Animations


data GameEnv = GameEnv {
  _gameSnds    :: HIO.LinearHashTable Int Music,
  _gameTxtr    :: HIO.LinearHashTable Int Texture,
  _gameSprts   :: HIO.LinearHashTable Int Sprite,
  _gameAnims   :: HIO.LinearHashTable Int Animation,
  _gameText    :: GText
}
makeLenses ''GameEnv

startEnv = do
  snds  <- HIO.new
  txtrs <- HIO.new
  sprs  <- HIO.new
  anims <- HIO.new
  gt <- createGameText
  return $ GameEnv snds txtrs sprs anims gt


addSound gEnv name = do
  snd <- err $ musicFromFile $ soundPath ++ name
  sId <- getMaxId (gEnv^.gameSnds) 
  HIO.insert (gEnv^.gameSnds) (sId+1)snd
  return sId

getSound gEnv i = HIO.lookup (gEnv^.gameSnds) i

class Anim a => EnvAnim a where
  addAnim :: GameEnv -> Int -> a ->  IO ()
  addAnim' :: GameEnv -> a ->  IO Int
  addAnimJ :: GameEnv -> a ->  IO ()
  addAnimJ gEnv = discard (addAnim' gEnv)
  rmvAnim :: GameEnv -> Int -> a ->  IO ()
  getAnim :: GameEnv -> Int -> IO (Maybe a)
  
addTxtr gEnv tId t = HIO.insert (gEnv^.gameTxtr) tId t
addTxtr' gEnv t = do
  aId <- getMaxId (gEnv^.gameTxtr) 
  HIO.insert (gEnv^.gameTxtr) (aId+1) t
  return aId

addSpr gEnv sId s = HIO.insert (gEnv^.gameSprts) sId s
addSpr' gEnv s = do
  sId <- getMaxId (gEnv^.gameSprts) 
  addSpr gEnv sId s
  return sId

instance EnvAnim Animation where
  addAnim gEnv aId a = do
    HIO.insert (gEnv^.gameAnims) aId a 
  addAnim' gEnv a = do
    aId <- getMaxId (gEnv^.gameAnims) 
    addAnim gEnv (aId+1) a
    return (aId+1)
  rmvAnim gEnv i anim = do
    HIO.delete (gEnv^.gameAnims) i
    cleanAnim anim
  getAnim gEnv i = HIO.lookup (gEnv^.gameAnims) i

instance EnvAnim (VTA Sprite) where
  addAnim gEnv aId (VTA a) = do
    HIO.insert (gEnv^.gameSprts) aId a 
  addAnim' gEnv a = do
    aId <- getMaxId (gEnv^.gameSprts) 
    addAnim gEnv (aId+1) a
    return (aId+1)
  rmvAnim gEnv i (VTA a) = do
    HIO.delete (gEnv^.gameSprts) i
    destroy a
  getAnim gEnv i = do
    s <- HIO.lookup (gEnv^.gameSprts) i
    s >?>= Nothing $ (return. (\a -> Just (VTA a)))
  

cleanEnv gEnv = do
  HIO.mapM_ (\(_,x) -> destroy x) $ gEnv^.gameSnds
  print "cleaning sounds"
  HIO.mapM_ (\(_,x) -> destroy x) $ gEnv^.gameTxtr
  print "cleaning txtrs"
  HIO.mapM_ (\(_,x) -> destroy x) $ gEnv^.gameSprts
  print "cleaning sprts"
  HIO.mapM_ (\(_,x) -> cleanAnim x) $  gEnv^.gameAnims
  print "cleaning anims"
  cleanGameText $ gEnv^.gameText
  print "cleaning texts"
{-# LANGUAGE OverloadedStrings,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module GameText where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable

import Data.IORef

import Lib
import Conf
import View

-- this is bad but not too much of a hassle
-- making the font polymorphic fucks everything up
data GFont  = MenuFont
            | DragonFont
            deriving  (Show,Read,Eq,Ord,Generic)

instance Hashable GFont

mkFont gfont = do
  ft <- err $ fontFromFile $ (fontPath ++ (show gfont) ++ ".ttf") 
  return ft

data GText = GText {
  _fonts  :: [GFont],
  _texts  :: HIO.LinearHashTable Int Text,
  _fontDr :: DHS.HashMap GFont Font
}
makeLenses ''GText


createTxt :: GText -> Int -> String -> GFont ->  Maybe Int -> Maybe Color -> IO ()
createTxt gt tId txt gfont size color = do
  let fnt = (gt^.fontDr) ! gfont
  tx <- err $ createText
  setTextFont tx fnt
  setTextString tx txt
  size >?> setTextCharacterSize tx
  color >?> setTextColor tx
  HIO.insert (gt^.texts) tId tx

deleteTxt tId gt = do
  t <- getText tId gt
  HIO.delete (gt^.texts) tId
  t >?> destroy 
  
{-# INLINE getText #-}
getText tId gt = HIO.lookup (gt^.texts) tId

cleanGameText gt = do
  mapM_ (\(_,f) -> destroy f)  $ DHS.toList $ gt^.fontDr
  HIO.mapM_ (\(k,v) -> destroy v) $ gt^.texts
  
createGameText = do
  let gfonts = [MenuFont]
  fts <- mapM (\gf -> do 
                            f <- mkFont gf
                            return $ (gf,f)) gfonts
  t <- HIO.new
  return $ GText gfonts t (DHS.fromList fts)  

{-# INLINE withGT #-}
withGT gt mf = do
    mf gt
    cleanGameText gt


instance Viewable Text where
  drawWithTransform tg transf t = do
    draw tg t $ Just $  renderStates {transform=transf}
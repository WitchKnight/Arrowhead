{-# LANGUAGE NondecreasingIndentation,LambdaCase,ScopedTypeVariables,OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module App where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import System.Random

import System.IO.Unsafe

import Debug.Trace
import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Control.Concurrent

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable
import Data.List(find,elemIndex)
import Data.IORef
import Control.Concurrent.KazuraQueue

import Lib
import Conf
import View
import GameText
import GameData
import Animations
import UI
import Game

import Actors







data MenuS = MenuS {
  _sMenu    :: GMenu, 
  _msTime   :: IORef Time,
  _msCursor  :: Maybe (V2 Int)
}

data BattleS = BattleS {
  _bsTime   ::  IORef Time,
  _player   ::  Player,
  _boss     ::  Boss,
  _pproj    ::  HIO.LinearHashTable Int Projectile ,
  _bproj    ::  HIO.LinearHashTable Int Projectile 
}




type BMenu = Menu MGame BattleS
type GMenu = Menu MGame MenuS
type GUI   = UIElem MGame MenuS
type MGame = (GameS MenuS)

data GameS a where
  InMenu ::   { mcode :: Maybe Int, 
                mwindow ::RenderWindow, 
                menv :: GameEnv, 
                mscene :: MenuS 
              }  -> GameS MenuS
  InBattle ::   { bcode :: Maybe Int, 
                  bwindow ::RenderWindow, 
                  benv :: GameEnv, 
                  bscene :: BattleS
                }  -> GameS BattleS





makeLenses ''BattleS
makeLenses ''MenuS


instance Scene MenuS where
  getTimeRef (MenuS _ t _) =  t
  drawWithEnv tg env s@(MenuS menu t _) = do
    HIO.mapM_ f (menu^.mElems)
    where
      f (crd,el) =  do
        time <- getTime s
        let V2 x y = getUIPos crd 
        let transf =  (translation x y)
        (el^.uiLabel) >?> \x -> do
          t <- getText (x^.lId) (env^.gameText)
          t >?> drawWithTransform tg transf
        dwtat tg time transf el

instance Scene BattleS where
  getTimeRef (BattleS t _ _ _ _) = t
  drawWithEnv tg gEnv s@(BattleS t p b pp bp) = do
    let drawKey i = do
        
        let V2 x y = V2 300 240
        let V2 x' y' = case i of
                        4 -> V2 (x-30) y
                        6 -> V2 (x+30) y
                        8 -> V2 x (y-30)
                        2 -> V2 x y
        let transf = scaling 2 2 * translation x' y'
        t <- getText i (gEnv^.gameText)
        t >?>/ (print "no text found") $ dwt tg transf
    mapM_ drawKey [2,4,6,8]

    (lifeAnim :: Maybe Animation) <- getAnim gEnv 1
    lifeAnim >?>/ (print "no anim") $ \a -> do 
      t <- getTime s
      l <- getLife p
      forM_ [1..l] $ \n -> do
        let n' = fromIntegral n
        let transf =  (translation ((11+n')*tSZ') (23*tSZ'))
        dwtat tg t transf a
      
    (bossAnim :: Maybe Animation) <- getAnim gEnv 2
    bossAnim >?>/ (print "no anim") $ \a -> do 
      t <- getTime s
      l <- getLife b
      getPhase b >>= \case
        Death -> do
          return ()
        Phase health _ _ _ -> do
          let phHealth = l - health
          when (phHealth > 0) $ do
            forM_ [1..(phHealth`div` 10)] $ \n -> do
              let n' = fromIntegral n
              let transf =  (translation ((11+n')*tSZ') (10*tSZ'))
              dwtat tg t transf a
        _ -> return ()

    let drawPProj proj = do
        AS (hst,ht) (wst,wt) angle d <- getState proj
        let href = p^.pProj.hrAnim
        hi <- HIO.lookup href hst
        V2 x y <- getPos proj
        let power  = clamp 0.5 3 $ proj^.rPow
        let transf =  (translation (x*tSZ') (y*tSZ'))* rotation angle * scaling power power
        let disp t i= do
            (anim :: Maybe Animation) <- getAnim gEnv i
            anim >?>/ (print "no anim") $ \a -> do 
              dwtat tg t transf a 
        hi >?>/ (print "no href !") $ disp ht
        return ()
    let drawBProj proj = do
        let n  = proj^.rNum
        let href = ((b^.bProjs) !! n)^.hrAnim


        AS (hst,ht) (wst,wt) angle d <- getState proj
        
        hi <- HIO.lookup href hst
        V2 x y <- getPos proj
        let power  = clamp 0.5 3 $ proj^.rPow
        let transf =  (translation (x*tSZ') (y*tSZ'))* rotation angle * scaling power power
        let disp t i= do
            (anim :: Maybe Animation) <- getAnim gEnv i
            anim >?>/ (print "no anim") $ \a -> do 
              dwtat tg t transf a 
        hi >?>/ (print "no href !") $ disp ht
        return ()
    drawActor tg gEnv b
    drawActor tg gEnv p
    HIO.mapM_ (drawPProj.snd) pp
    HIO.mapM_ (drawBProj.snd) bp

  updateScene dt bS@(BattleS t p b pp bp) = do 
    addTime dt bS
    updateActor dt p

    turnScreen p
    turnScreen b

    getPhase b >>= \case
      Death -> HIO.mapM_ (HIO.delete bp.fst) bp >> return bS
      Transition _ -> return bS
      Phase health scram (BAI ai) next -> do
        t' <- getTime bS
        updateActor dt b
        ai dt p b
        (p^.pAuto) t' b p

        let movePP = (p^.pProj.rMove)  p b dt
        let moveBP proj = do
            let n = proj^.rNum
            let move = ((b^.bProjs) !! n)^. rMove
            move p b dt proj
      
        let updateProj dt proj = updateActor dt proj >> modifyIORef (proj^.rTime) (+dt)

        HIO.mapM_ (updateProj dt.snd) pp
        HIO.mapM_ (updateProj dt.snd) bp
        HIO.mapM_ (movePP.snd) pp
        HIO.mapM_ (moveBP.snd) bp

        
        
        let onHitBoss proj = (p^.pProj^.onHit) proj
        let onHitPlayer proj = (((b^.bProjs) !! (proj^.rNum))^.onHit) proj
        let touchBoss proj= do
            hState proj >>= \case
              Okay -> do
                bp <- getPos b
                prp <- getPos proj
                let power = proj^.rPow
                let prp' = fmap (*tSZ') prp
                let bp' = fmap (*tSZ') bp
                return $ touches prp' bp' (V2 46 46)
              _   -> 
                return False
        let touchPlayer proj = do
            hState proj >>= \case
              Okay -> do
                prp <- getPos proj
                pp <- getPos p
                let size = proj^.rSize
                let power = proj^.rPow
                let prp' = fmap (*tSZ') prp
                let pp' = fmap (*tSZ') pp
                return $ touches prp' pp' $ fmap (size*) ctSZ
              _ -> return False
        let boomBoss proj = do
            touch <- touchBoss proj
            when touch $ do
              let power = proj^.rPow
              onHitBoss proj b
              modState proj explode
        let boomPlayer proj = do
            touch <- touchPlayer proj
            when touch $ do
              let power = proj^.rPow
              onHitPlayer proj p 
              modState proj explode

        
        let checkDeath prs (i,proj) = do
              l <- getLife proj
              pos <- getPos proj
              t <- readIORef $ proj^.rTime
              st <- hState proj
              when (asSeconds t > 15 && st == Okay) $ do
                modState proj (hardHState Hurt)
              when (l<1 || outOfBounds pos) $ do
                HIO.delete prs i

        HIO.mapM_ (boomBoss.snd) pp
        HIO.mapM_ (boomPlayer.snd) bp
        HIO.mapM_ (checkDeath pp) pp
        HIO.mapM_ (checkDeath bp) bp
        return bS

instance GameState (GameS MenuS) MenuS where
  getOCode (InMenu c _ _ _) = c
  noCode (InMenu _ w env scene) = (InMenu Nothing w env scene)
  withOCode (InMenu _ w env scene) c = (InMenu (Just c) w env scene)
  envG (InMenu _ _ env _) = env
  scenG (InMenu _ _ _ s)  = s
  wndG (InMenu _ w env scene) = w
  withSc s (InMenu c w env scene) = (InMenu c w env s)

instance GameState (GameS BattleS)  BattleS where
  getOCode (InBattle c _ _ _) = c
  noCode (InBattle _ w env scene) = (InBattle Nothing w env scene)
  withOCode (InBattle _ w env scene) c = (InBattle (Just c) w env scene)
  envG (InBattle _ _ env _) = env
  scenG (InBattle _ _ _ s)  = s
  wndG (InBattle _ w env scene) = w
  withSc s (InBattle c w env scene) = (InBattle c w env s)
  updateGame dt gS@(InBattle c w env s@(BattleS t player boss pp bp)) = do
    sc <- updateScene dt s
    let ngS = withSc sc gS 
            
        
    let (bQ :: Queue Int) = boss^.bQueue
    lQ <- lengthQueue bQ

    when (lQ> 0) $ do
      n <- readQueue bQ
      playProjSound env n 
      spawnBossProj n (boss^.bProjs) s



    getPhase boss >>= \case
      Transition phase -> do
        t<- getTime s
        scramblePlayer t player boss
        threadDelay 20000
        HIO.mapM_ (\(i,p) -> HIO.delete bp i) bp
        let f i= do
            t <- getText i (env^.gameText) 
            s <- HIO.lookup (player^.pKeys) i
            s >?> \s -> do
              let text = case s `elem` lastKeys of 
                            True -> '?'
                            False -> last $ show s 
              t >?> \t -> setTextString t [text]
        mapM f [2,4,6,8]
        setPhase boss phase
      _ -> return ()
    (getLife player >>= \l -> return (l>1)) >>= \case
      False ->  return $ withOCode ngS 1
      _ ->  getPhase boss >>= \case 
        Death -> return $ withOCode ngS 2
        _ -> return ngS
cancelMenu :: (GameS MenuS) -> IO ()
cancelMenu g@(InMenu _ w env (MenuS m t _)) = do
  (m^.cancelAct) >?> execAction g

confirmMenu :: (GameS MenuS) -> IO (GameS MenuS)
confirmMenu g@(InMenu _ w env s@(MenuS m t _)) = do
  elems <- HIO.toList $ m^.mElems
  let activeElems = (filter (^.uiActive) $ map snd elems)
  let filtSel = \el@UIElem{uiSelect = f} ->  f s
  selecElems <- filterM filtSel activeElems
  nG <- case selecElems of
    [] ->(m^.defaultAct) >?>= g $ \a -> print "fm" >> mapAction g a
    _  -> return g
  let knot g el@UIElem{uiAction = a} = a >?>= g $  \a -> print "sm" >> mapAction g  a               
  print selecElems
  cG <- foldM knot nG selecElems 
  return cG

switchTo :: (GameEnv -> IO GMenu) -> GameS MenuS -> IO (GameS MenuS) 
switchTo mkMenu (InMenu c w gEnv menuS)= do
  print "changing menu"
  let oldMenu = menuS^.sMenu
  cleanMenu oldMenu gEnv
  newMenu <- mkMenu gEnv
  let nS = switchMenu newMenu menuS
  return (InMenu c w gEnv nS)

cursorInRange crd howMuch wL s  = do
  let c = s^.msCursor
  res <- case c of 
    Nothing  -> return False
    Just (V2 x y)  -> do
      let p = getUIPos crd
      return $ (distance (V2 (fromIntegral x-(wL)) (fromIntegral y)) p < howMuch)
  return res

updateCursor x y s =
  return $ s & msCursor .~ Just (V2 x y)

switchMenu newMenu s = s & sMenu.~ newMenu

-- Battle Stuff

mkBossProj  :: Coord -> Int -> Float -> Boss -> IO Projectile
mkBossProj target n pow boss = do
  pos <- getPos boss
  let info = (boss^.bProjs) !! n
  state <- newIORef $ AS (Okay,0) (Idle,0) 0 []
  life <- newIORef 1
  rPos <-newIORef pos 
  time <- newIORef 0
  speed <- newIORef $ info^.sSpd
  vec <- newIORef $ target - pos
  let size = case n of 
              0 -> 1.3
              1 -> 1.2
              _ -> 0.8


  return $ Projectile n size life rPos vec state time speed pow

mkPlayerProj :: Float -> Player -> IO Projectile
mkPlayerProj pow player = do
  pos <- getPos player
  let info = player^.pProj
  state <- newIORef  $  AS (Okay,0) (Idle,0) 0 []
  life <- newIORef 1
  rPos <-newIORef pos
  time <- newIORef 0
  speed <- newIORef $ info^.sSpd
  bPos <- newIORef c0 
  return $ Projectile 0 1 life rPos bPos state time speed pow
  
playProjSound gEnv n = do
  let a =  case n of  
                  0 -> 5
                  1 -> 4
                  5 -> 3
                  _ -> 2
  s <- getSound gEnv a
  s >?> play


lastKeys = [KeyLControl,KeyRAlt,KeyDelete,KeyTab]

spawnPlayerProj pow s@(BattleS time player boss pp _ ) = do
  pId <- getMaxId pp
  proj<- mkPlayerProj pow player
  faceBoss boss proj
  HIO.insert pp (pId+1) proj

 

spawnBossProj n projs s@(BattleS time player boss _ bp ) = do
  pos <- getPos boss
  ppos <- getPos player
  pId <- getMaxId bp
  return ()
  return n >>= \case
        0 -> do
          let pow  = (projs !! n)^.bPow
          proj<- mkBossProj ppos n pow boss
          faceBoss player proj
          HIO.insert bp (pId+1) proj
          
        1 -> do
          let pow  = (projs !! n)^.bPow
          proj<- mkBossProj ppos n pow boss
          faceBoss player proj
          HIO.insert bp (pId+1) proj
        2 -> do
          let pow  = (projs !! 2)^.bPow
          rx <- randomRIO (-10,10)
          ry <- randomRIO (-10,10)
          proj<- mkBossProj (pos + V2 rx ry) n pow boss
          faceBoss player proj
          HIO.insert bp (pId+1) proj
        3 -> do
          let pow  = (projs !! 2)^.bPow
          forM_ [n  | n <- [-10..10], (truncate n) `rem` 2 ==0] $ \n -> do
            pId <- getMaxId bp
            proj<- mkBossProj (ppos + V2 (n*2) (-n*2)) 2 pow boss
            faceBoss player proj
            HIO.insert bp (pId+1) proj
        4 -> do
          let pow  = (projs !! 2)^.bPow
          forM_ [-10..10] $ \n -> do
            rx <- randomRIO (-10,10)
            ry <- randomRIO (-10,10)
            pId <- getMaxId bp
            proj<- mkBossProj (pos + V2 (rx+n*2) (ry-n*2)) 2 pow boss
            faceBoss player proj
            HIO.insert bp (pId+1) proj
        5 -> do
          let pow  = (projs !! 2)^.bPow
          forM_ [-8..7] $ \n -> do
            rx <- randomRIO (-10,10)
            ry <- randomRIO (-10,10)
            pId <- getMaxId bp
            proj<- mkBossProj (pos + V2 (rx+n*2) (ry-n*2)) 0 pow boss
            faceBoss player proj
            HIO.insert bp (pId+1) proj



  



scramblePlayer time player boss = do
  getPhase boss >>= \case
    Transition (Phase health keys _ _) -> do
      forM_ [2,4,6,8] $ \dir -> modState player (rmDir dir)
      forM_ [2,4,6,8] $ (HIO.delete (player^.pKeys))
      newKeys <- getRandomList 4 keys
      let newKeyRaw =  zip [2,4,6,8] newKeys
      forM_ newKeyRaw $ \(i,k) -> print k >> HIO.insert (player^.pKeys) i k 
      
    _ -> return ()



handlePress :: KeyCode -> (GameS BattleS) -> IO ()
handlePress code gS@(InBattle _ wnd env s@(BattleS time player boss _ _ )) = do
  let keys = player^.pKeys
  let check dir = do
      k <- HIO.lookup keys dir
      k >?> \c -> do
        when (c == code) $
          modState player (addDir dir)
  mapM_ check [2,4,6,8]
  when (code == KeySpace) $ do
    AS _ (wst,t) _ _ <- getState player
    when (wst /= Charging) $ do
      modState player beginCharge
  return ()

handleRelease :: KeyCode -> (GameS BattleS) -> IO ()
handleRelease code gS@(InBattle _ wnd env s@(BattleS time player boss _ _)) = do
  let keys = player^.pKeys
  let check dir = do
      k <- HIO.lookup keys dir
      k >?> \c -> do
        when (c == code) $
          modState player (rmDir dir)
  mapM_ check [2,4,6,8]
  when (code == KeySpace) $ do
    AS _ (wst,t) _ _ <- getState player
    modState player stopCharge
    
    spawnPlayerProj (asSeconds t) s
  return ()


{-# LANGUAGE NondecreasingIndentation,RankNTypes,LambdaCase,ScopedTypeVariables,OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Actors where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time


import Debug.Trace
import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable
import Data.List(find,elemIndex)
import Data.IORef
import Control.Concurrent.KazuraQueue

import Lib
import Conf
import View
import GameText
import GameData
import Animations
import UI
import Game

data Projectile = Projectile {
  _rNum   :: Int,
  _rSize  :: Float,
  _rLife  :: IORef Int,
  _rPos   :: IORef Coord,
  _rVec   :: IORef Coord,
  _rState :: IORef ActorState,
  _rTime  :: IORef Time,
  _rSpeed :: IORef Float,
  _rPow   :: Float
}

data ProjInfo actor = ProjInfo {
  _hrAnim ::  HIO.LinearHashTable HealthState Int,
  _bPow   ::  Float,
  _onHit  ::  Projectile -> actor -> IO (),
  _rMove  ::  Player -> Boss ->  Time -> Projectile ->IO (),
  _sSpd   ::  Float
}

data Player = Player {
  _pLife      :: IORef Int,
  _pPos       :: IORef Coord,
  _pKeys      :: HIO.LinearHashTable Int KeyCode,
  _pState     :: IORef ActorState,
  _wpAnim     :: HIO.LinearHashTable WpnState Int,
  _hpAnim     :: HIO.LinearHashTable HealthState Int,
  _pSpeed     :: IORef Float,
  _pAuto      :: Time ->  Boss -> Player -> IO (),
  _pProj      :: ProjInfo Boss
}

data Boss = Boss {
  _bLife        :: IORef Int,
  _bPos         :: IORef Coord,
  _bState       :: IORef ActorState,
  _wbAnim       :: HIO.LinearHashTable WpnState Int,
  _hbAnim       :: HIO.LinearHashTable HealthState Int,
  _bSpeed       :: IORef Float,
  _bProjs       :: [ProjInfo Player],
  _bQueue       :: Queue Int,
  _bPhase       :: IORef Phase
}

data Phase = Phase {
  _phealth    :: Int,
  _scramKeys  :: [KeyCode],
  _pAI        :: BossAI,
  _nextPh     :: Phase
} | Death | Transition Phase
-- bottom health of the boss in the phase, 
-- codes possible for the scramble,
-- next phase.




newtype BossAI =  BAI (Time -> Player -> Boss -> IO ())

type Direction = [Int]

type Angle = Float

data WpnState = Idle
              | Charging 
              deriving (Show,Read,Eq,Ord,Generic)

instance Hashable WpnState

data ActorState = AS (HealthState,Time) (WpnState,Time) Angle Direction

data HealthState  = Okay
                  | Hurt 
                  deriving (Show,Read,Eq,Ord,Generic)

instance Hashable HealthState

makeLenses ''Player
makeLenses ''Projectile
makeLenses ''ProjInfo
makeLenses ''Boss
makeLenses ''Phase

class Moving a where
  speed   :: a -> IORef Float
  pos     :: a -> IORef Coord
  
  getPos  :: a -> IO Coord
  getPos a = readIORef $ pos a
  setPos  :: a -> Coord -> IO ()
  setPos a n = writeIORef (pos a) n
  modPos  :: a -> (Coord -> Coord) -> IO ()
  modPos a = modifyIORef (pos a)
  getSpeed  :: a -> IO Float
  getSpeed a = readIORef $ speed a
  setSpeed  :: a -> Float -> IO ()
  setSpeed a n = writeIORef (speed a) n
  modSpeed  :: a -> (Float -> Float) -> IO ()
  modSpeed a = modifyIORef (speed a) 

class Moving a => Actor a where
  life    :: a -> IORef Int
  state   :: a -> IORef ActorState

  anHRef  :: a -> HIO.LinearHashTable HealthState Int
  anWRef  :: a -> HIO.LinearHashTable WpnState Int
  
  getLife :: a -> IO Int
  getLife a = readIORef $ life a
  setLife :: a -> Int -> IO ()
  setLife a n = writeIORef (life a) n
  modLife :: a -> (Int -> Int) -> IO ()
  modLife a = modifyIORef (life a)

  hurt :: a -> Int -> IO ()
  hurt a amount= undefined

  getState  :: a -> IO ActorState
  getState a = readIORef $ state a
  setState  :: a -> ActorState -> IO ()
  setState a n = writeIORef (state a) n
  modState  :: a -> (ActorState -> ActorState) -> IO ()
  modState a = modifyIORef (state a)
  



  updateActor :: Time -> a -> IO ()
  updateActor dt actor = do
    let updateState (AS (hst,ht) (wst,st) a d) = AS (hst,ht+dt) (wst,st+dt) a d
    AS (hst,ht) (wst,st) a d<- getState actor
    spd <- getSpeed actor
    let spd' = case wst of
                Charging   -> spd/2
                _          -> spd
    return hst >>= \case
      Hurt -> do
        when (asSeconds ht > 0.1) $ do
          modLife  actor (subtract 1)
          modState actor (hardHState Okay)
      _     -> return ()
    modPos actor ( + (getMove spd' dt d))
    modState actor updateState


  drawActor :: SFRenderTarget b => b -> GameEnv -> a -> IO ()
  drawActor tg gEnv actor = do
    AS (hst,ht) (wst,wt) angle d<- getState actor
    let href = anHRef actor
    let wref = anWRef actor
    hi <- HIO.lookup href hst
    wi <- HIO.lookup wref wst
    pos@(V2 x y) <- getPos actor
    let transf =  (translation (x*tSZ') (y*tSZ'))* rotation angle
    let disp t i= do
        (anim :: Maybe Animation) <- getAnim gEnv i
        anim >?>/ (print "no anim") $ dwtat tg t transf
    hi >?> disp ht
    wi >?> disp wt


instance Moving Player where
  pos     = (^.pPos)
  speed   = (^.pSpeed)

instance Moving Boss where
  pos     = (^.bPos)
  speed   = (^.bSpeed)

instance Actor Player where
  life    = (^.pLife)
  state   = (^.pState)
  anWRef  = (^.wpAnim)
  anHRef  = (^.hpAnim)
  
instance Actor Boss where
  life    = (^.bLife)
  state   = (^.bState)
  anWRef  = (^.wbAnim)
  anHRef  = (^.hbAnim)

instance Moving Projectile where
  pos     = (^.rPos)
  speed   = (^.rSpeed)

instance Actor Projectile where
  life    = (^.rLife)
  state   = (^.rState)
  anWRef  = undefined
  anHRef  = undefined
  





faceBoss :: (Actor a, Actor b) =>  b -> a -> IO  ()
faceBoss b p = do
  pp@(V2 px py) <- getPos p
  bp@(V2 bx by) <- getPos b
  let angle = (+) 90 $  (/pi) $ 180 * atan2 (by-py) (bx-px)
  hs <- hState p
  when (hs == Okay) $  modState p (setAngle angle)
  




addDir :: Int -> ActorState -> ActorState 
addDir dir stat@(AS (hst,ht) (wst,st) a dirs) 
  | dir `elem` dirs =  stat
  | otherwise = (AS (hst,ht) (wst,st) a $ dir:dirs) 


rmDir :: Int -> ActorState -> ActorState 
rmDir dir stat@(AS (hst,ht) (wst,st) a dirs) = (AS (hst,ht) (wst,st) a [ d | d<- dirs, d/=dir]) 

beginCharge stat@(AS (hst,ht) (wst,st) a dirs) = (AS (hst,ht) (Charging,0) a dirs)

stopCharge stat@(AS (hst,ht) (wst,st) a dirs) = (AS (hst,ht) (Idle,0) a dirs)

resetWpn (AS (hst,ht) (wst,st) a dirs) =(AS (hst,ht) (wst,0) a dirs) 
resetHth (AS (hst,ht) (wst,st) a dirs) =(AS (hst,0) (wst,st) a dirs) 

oppos 4 = 6
oppos 6 = 4
oppos 2 = 8
oppos 8 = 2
oppos _  = 5



switchDir actor dir = do
  modState actor (addDir dir)
  modState actor (rmDir $ oppos dir)

hState p = do
  (AS (hst,ht) (wst,st) a dirs) <- getState p
  return hst

getMove :: Float -> Time -> Direction -> Coord
getMove speed dt dirs = fmap (*(dt'*speed)) $ dirvec
  where
    dirvec = foldr (+) c0 $ map getMove' dirs 
    dt' = asSeconds dt
    getMove' 4 = V2 (-1) 0
    getMove' 6 = V2 1 0
    getMove' 2 = V2 0 1
    getMove' 8 = V2 0 (-1)

setAngle angle stat@(AS (hst,ht) (wst,st) a dirs)   = (AS (hst,ht) (wst,st) angle dirs)
modAngle f stat@(AS (hst,ht) (wst,st) a dirs)   = (AS (hst,ht) (wst,st) (f a) dirs)
explode stat@(AS (hst,ht) (wst,st) a dirs)   = (AS (Hurt,0) (wst,st) a dirs)
hardHState state stat@(AS (hst,ht) (wst,st) a dirs) = (AS (state,0) (wst,st) a dirs)
softHState state stat@(AS (hst,ht) (wst,st) a dirs) = (AS (state,ht) (wst,st) a dirs)

switchAI boss newAI = do 

  modPhase boss (setAI (newAI))

getPhase boss = readIORef (boss^.bPhase)
modPhase boss = modifyIORef (boss^.bPhase)
setPhase boss = writeIORef (boss^.bPhase)
setAI ai (Phase health scram (BAI _) next) = Phase health scram (BAI ai) next
setAI ai (Transition (Phase health scram (BAI _) next)) = Transition $ Phase health scram (BAI ai) next
setAI _ other = other


turnScreen act = do
  (V2 x y) <- getPos act
  when (x < 8) $ modPos act (\(V2 x y) -> (V2 ((gWIDTH'-1)/23-10) y) )
  when (x > ((gWIDTH'-1)/23-9)) $ modPos act (\(V2 x y) -> (V2 10 y) )
  when (y < 8) $ modPos act (\(V2 x y) -> (V2 x ((gHEIGHT'-1)/23-10) ))
  when (y > ((gHEIGHT'-1)/23-9)) $ modPos act (\(V2 x y) -> (V2 x 10))

{-# LANGUAGE OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}

module Main where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Clock

import Control.Monad
import Control.Concurrent

import System.IO.Unsafe
import Lib
import Game
import Animations
import Conf
import View
import GameData
import Game

import Init
import App

main :: IO ()
main = do
  let ctxSettings = Just $ ContextSettings 24 8 0 1 2 [ContextDefault]
  wnd   <- createRenderWindow (VideoMode gWIDTH gHEIGHT 32) "A R R O W H E A D" [SFDefaultStyle] ctxSettings
  setVSync wnd True
  clock <- createClock
  gS    <- initGame wnd
  loop clock timeZero gS
  cleanGame gS
  return ()


loop :: Clock -> Time -> GameS a ->  IO ()
loop clock dt gS@(InMenu c wnd env scene) = case c of
  Nothing -> do
    dt    <- restartClock clock
    gS <- updateGame dt gS
    drawGame gS
    evt   <- pollEvent wnd
    case evt of
      Just SFEvtClosed -> return ()
      Just (SFEvtMouseMoved x y) -> do
        s <- updateCursor x y scene
        loop clock dt (withSc s gS)
      Just (SFEvtMouseButtonPressed p x y) -> do
        gS <- confirmMenu gS
        loop clock dt gS
      _                -> loop clock dt gS
  Just x -> do
          when (x==1) $ do
            gS <- initBattle gS
            loop clock dt $ noCode gS
          when (x==0) $ do
            print "quitting game..."

loop clock dt gS@(InBattle c wnd env scene) = case c of
  Nothing -> do
    dt    <- restartClock clock
    gS <- updateGame dt gS
    drawGame gS
    evt   <- pollEvent wnd
    case evt of
      Just SFEvtClosed -> return ()
      Just (SFEvtKeyPressed code _ _ _ _) -> do
        handlePress code gS
        loop clock dt gS
      Just (SFEvtKeyReleased code _ _ _ _) -> do
        handleRelease code gS
        loop clock dt gS
      -- Just (SFEvtMouseButtonPressed p x y) -> do
      --   gS <- confirmMenu gS
      --   loop clock dt gS
      _                -> loop clock dt gS
  Just x -> do
          when (x==0) $ do
            print "quitting game..."
          when (x==1) $ do
            gS    <- gameOver gS
            print "game over.."
            loop clock dt gS
          when (x==2) $ do
            gS <- gameFinished gS
            print "well done.."
            loop clock dt gS
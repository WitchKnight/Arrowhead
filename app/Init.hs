{-# LANGUAGE LambdaCase,ScopedTypeVariables,NondecreasingIndentation,TemplateHaskell,DoAndIfThenElse, GADTs,TypeSynonymInstances, FlexibleInstances,DeriveGeneric,DefaultSignatures #-}

module Init where

import GHC.Generics

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import System.IO.Unsafe
import System.Random

import Data.HashMap.Strict((!))

import qualified Data.HashMap.Strict as DHS
import qualified Data.HashTable.IO as HIO
import Data.Hashable
import Linear.V2
import Data.IORef
import Control.Concurrent.KazuraQueue

import Control.Lens
import Control.Monad

import Game
import GameText
import GameData
import Lib
import Animations
import Conf
import UI
import View

import App
import Actors


-- MENU STUFF

initGame wnd = do
  gEnv <- startEnv

  menu <- mainMenu gEnv

  addSound gEnv "strike.ogg"
  addSound gEnv "pew.ogg"
  addSound gEnv "wave.ogg"
  addSound gEnv "lilarrow.ogg"
  addSound gEnv "bigarrow.ogg"
  addSound gEnv "gameover.ogg"
  addSound gEnv "Paradox.ogg"
  addSound gEnv "theme.ogg"
  addSound gEnv "triumph.ogg"

  t <- newIORef timeZero

  s' <- getSound gEnv 8
  s' >?> play


  return $ InMenu  Nothing wnd gEnv (MenuS menu t Nothing)



basicMenu cels gEnv = do
  cels' <- mapM (\(x,y') -> do
                              y <- y'
                              return (x,y)) $ mkMenuEls cels
  initMenu Nothing Nothing cels' gEnv
  


mkMenuEls :: [(UICoord,Int -> IO GUI)] -> [(UICoord,IO GUI)]
mkMenuEls stuff = mkMenuEls' 0 stuff        
mkMenuEls' _ [] = []             
mkMenuEls' n ((crd,mkEl):xs) = (crd, newEl):(mkMenuEls' (n+1) xs)
  where
    newEl = do
      el <- mkEl n
      wordSize <- (el^.uiLabel) >?>= (0 :: Float) $ \l -> do
        return $  fromIntegral $ (`div` 2) $ (length  $ l^.lText) * maybeOr (l^.cSize) 30
      return $ el {uiSelect = cursorInRange crd (wordSize/2) (wordSize/2) }


mainMenu :: GameEnv -> IO (GMenu)
mainMenu = basicMenu [ 
  ((V2 (V2 3 7) (V2 1 7)),  mkPlainText "A R R O W H E A D" (Just 50)),
  ((V2 (V2 1 2) (V2 1 3)),  mkActionText (OutsideAction 1) "Play" (Just 43)),
  ((V2 (V2 1 2) (V2 3 6)),  mkActionText (OutsideAction 0) "Quit" (Just 43))
  ]

overMenu :: GameEnv -> IO (GMenu)
overMenu = basicMenu [ 
  ((V2 (V2 3 7) (V2 1 7)),  mkPlainText "GAME OVER" (Just 60)),
  ((V2 (V2 1 2) (V2 1 3)),  mkActionText (OutsideAction 1) "Try Again" (Just 43)),
  ((V2 (V2 1 2) (V2 3 6)),  mkActionText (OutsideAction 0) "Quit" (Just 43)) 
  ]


endMenu :: Time -> GameEnv -> IO (GMenu)
endMenu time = basicMenu [ 
  ((V2 (V2 1 7) (V2 1 7)),  mkPlainText ("CONGRATULATIONS !") (Just 60)),
  ((V2 (V2 2 7) (V2 2 7)),  mkPlainText ("Total time : " ++ (show $ asSeconds time) ++ " seconds") (Just 40)),
  ((V2 (V2 1 2) (V2 1 3)),  mkActionText (OutsideAction 1) "Try Again ?" (Just 43)),
  ((V2 (V2 1 2) (V2 3 6)),  mkActionText (OutsideAction 0) "Quit" (Just 43)) 
  ]


gameFinished :: GameS BattleS -> IO  (GameS MenuS) 
gameFinished (InBattle _ w gEnv bS) = do
  v <- getView w
  resetView v $ FloatRect 0 0 gWIDTH' gHEIGHT'
  setView w v
  t <- getTime bS
  menu <- endMenu t gEnv
  s <- getSound gEnv 9
  s >?> play
  s' <- getSound gEnv 7
  s' >?> stop
  s' <- getSound gEnv 6
  s' >?> stop
  t' <- newIORef timeZero
  return $ InMenu  Nothing w gEnv (MenuS menu t' Nothing)




gameOver  :: GameS BattleS -> IO  (GameS MenuS) 
gameOver  (InBattle _ w gEnv bS) = do
  v <- getView w
  resetView v $ FloatRect 0 0 gWIDTH' gHEIGHT'
  setView w v
  menu <- overMenu gEnv
  t <- newIORef timeZero
  s <- getSound gEnv 6
  s >?> play
  s' <- getSound gEnv 7
  s' >?> stop
  return $ InMenu  Nothing w gEnv (MenuS menu t Nothing)


-- BATTLE STUFF

mkPlayer :: GameEnv -> [(HealthState,Animation)] ->[(WpnState,Animation)] -> [(HealthState,Animation)] ->  Coord -> IO Player
mkPlayer gEnv hanims wanims phanims coord = do
  rHAnims <- HIO.new 
  rWAnims <- HIO.new
  rPHAnims <- HIO.new
  forM hanims $ \(state,anim) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (rHAnims) state iD
  forM wanims $ \(state,anim) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (rWAnims) state iD
  forM phanims $ \(state,anim) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (rPHAnims) state iD
  apos <- newIORef coord
  stat <- newIORef  $ AS (Okay,0) (Idle,0) 0 []
  life <- newIORef 5
  speed<- newIORef 5
  keys <- HIO.fromList [
                        (2,KeyDown),
                        (4,KeyLeft),
                        (6,KeyRight),
                        (8,KeyUp)
                        ]
  let auto t= faceBoss
  let pInfo = ProjInfo {
    _bPow = 5,
    _sSpd   = 14,
    _hrAnim = rPHAnims,
    _onHit = \p boss -> modState boss (hardHState  Hurt) >> modLife boss (subtract (truncate $ 1 + (p^.rPow)**3.3)),
    _rMove = \player boss dt proj  -> do
      faceBoss boss proj
      target <- getPos boss
      current  <- getPos proj
      spd <- getSpeed proj
      let vec = target - current
      let norm = sqrt $ (vec^._x)**2 + (vec^._y)**2
      let unary = fmap (/norm) vec
      modPos proj (+ ( fmap (*(spd*asSeconds dt)) unary))
  }
  return $ Player life apos keys stat rWAnims rHAnims speed auto pInfo

moveU1 threshold nextProj time player boss = do 
  getPhase boss >>= \case
    Death -> return ()
    Phase health _ _ nextPhase -> do
      l <- getLife boss
      let phHealth = l - health
      when (phHealth < 0 ) $  do
        
        setPhase boss $ Transition nextPhase
      pp <- getPos player
      bp <- getPos boss
      AS (hst,ht) (wst,st) _ _ <- getState boss
      when (pp^._x < bp ^._x) $ do
        switchDir boss 4
      when (pp^._x > bp ^._x) $ do
        switchDir boss 6
      when (pp^._y < bp ^._y) $ do
        switchDir boss 8
      when (pp^._y > bp ^._y) $ do
        switchDir boss 2
      when (asSeconds st > threshold) $ do
        switchAI boss $ shootProj1 nextProj
      when (asSeconds ht > 0.3) $ do
        writeQueue (boss^.bQueue) 2
        modState boss resetHth
 
    
    


shootProj1 n time player boss = do
  writeQueue (boss^.bQueue) n
  modState boss resetWpn
  return n >>= \case
    0 -> do
      switchAI boss $ moveU1 (1) 1
    1 -> 
      switchAI boss $ moveU1 (2) 0

moveU2 threshold nextProj1 nextProj2 time player boss = do 
  getPhase boss >>= \case
    Death -> return ()
    Phase health _ _ nextPhase -> do
      l <- getLife boss
      let phHealth = l - health
      when (phHealth < 0 ) $  do
        
        setPhase boss $ Transition nextPhase
      pp <- getPos player
      bp <- getPos boss
      AS (hst,ht) (wst,st) _ _ <- getState boss
      when (pp^._x < bp ^._x) $ do
        switchDir boss 4
      when (pp^._x > bp ^._x) $ do
        switchDir boss 6
      when (pp^._y < bp ^._y) $ do
        switchDir boss 8
      when (pp^._y > bp ^._y) $ do
        switchDir boss 2
      when (asSeconds st > threshold) $ do
        switchAI boss $ shootProj2 nextProj1
      when (asSeconds ht > 0.2) $ do
        writeQueue (boss^.bQueue) nextProj2
        modState boss resetHth

shootProj2 n time player boss = do
  writeQueue (boss^.bQueue) n
  modState boss resetWpn
  return n >>= \case
    0 -> do
      switchAI boss $ moveU2 (1.8) 1 3 
    1 -> 
      switchAI boss $ moveU2 (1.5) 0 4
      


moveU3 threshold nextProj1 nextProj2 time player boss = do 
  getPhase boss >>= \case
    Death -> return ()
    Phase health _ _ nextPhase -> do
      l <- getLife boss
      let phHealth = l - health
      when (phHealth < 0 ) $  do
        setPhase boss $ Transition nextPhase
      pp <- getPos player
      bp <- getPos boss
      AS (hst,ht) (wst,st) _ _ <- getState boss
      let vec = pp - bp
      let norm = sqrt $ (vec^._x)**2 + (vec^._y)**2
      let unary = fmap (/norm) vec
      let spd = 1
      modPos boss (+ ( fmap (*((spd)*asSeconds time)) unary))
      when (pp^._x < bp ^._x) $ do
        switchDir boss 4
      when (pp^._x > bp ^._x) $ do
        switchDir boss 6
      when (pp^._y < bp ^._y) $ do
        switchDir boss 8
      when (pp^._y > bp ^._y) $ do
        switchDir boss 2
      when (asSeconds st > threshold) $ do
        switchAI boss $ shootProj3 nextProj1
      when (asSeconds ht > 0.3) $ do
        writeQueue (boss^.bQueue) 5
        modState boss resetHth

shootProj3 n time player boss = do
  writeQueue (boss^.bQueue) n
  modState boss resetWpn
  return n >>= \case
    0 -> do
      switchAI boss $ moveU3 (0.5) 1 1
    1 -> 
      switchAI boss $ moveU3 (1) 0 0
              


mkBoss :: GameEnv -> [(HealthState,Animation)] -> [(WpnState,Animation)] -> [[(HealthState,Animation)]] -> Coord -> IO Boss
mkBoss gEnv hanims wanims phanims coord = do
  rHAnims <- HIO.new 
  rWAnims <- HIO.new
  rPHAnims0<- HIO.new
  rPHAnims1 <- HIO.new
  rPHAnims2 <- HIO.new
  forM hanims $ \(state,anim) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (rHAnims) state iD
  forM wanims $ \(state,anim) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (rWAnims) state iD
  forM (phanims !! 0) $ \(state,anim) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (rPHAnims0) state iD
  forM (phanims !! 1) $ \(state,anim) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (rPHAnims1) state iD
  forM (phanims !! 2) $ \(state,anim) -> do
    iD <- addAnim' gEnv anim
    HIO.insert (rPHAnims2) state iD
  life <- newIORef 1000
  apos <- newIORef coord
  stat <- newIORef  $ AS (Okay,0) (Idle,0) 0 []
  speed<- newIORef 0.5


  let pInfo0 = ProjInfo {
    _bPow = 2,
    _sSpd   = 10,
    _hrAnim = rPHAnims0,
    _onHit = \p player -> modLife player (subtract 1),
    _rMove = \player boss dt proj  -> do
      spd <- getSpeed proj
      vec <- readIORef $ proj^.rVec
      let norm = sqrt $ (vec^._x)**2 + (vec^._y)**2
      let unary = fmap (/norm) vec
      modPos proj (+ ( fmap (*(spd*asSeconds dt)) unary))
  }
  let pInfo1 = ProjInfo {
    _bPow = 2,
    _sSpd   = 5,
    _hrAnim = rPHAnims1,
    _onHit = \p player-> modLife player (subtract 1),
    _rMove = \player boss dt proj  -> do
      modSpeed proj (\s -> s*(1- (asSeconds dt)/2))
      faceBoss player proj
      target <- getPos player
      current  <- getPos proj
      spd <- getSpeed proj
      let vec = target - current
      let norm = sqrt $ (vec^._x)**2 + (vec^._y)**2
      let unary = fmap (/norm) vec
      modPos proj (+ ( fmap (*((spd+0.2)*asSeconds dt)) unary))

  }
  let pInfo2 = ProjInfo {
    _bPow = 2,
    _sSpd   = 2,
    _hrAnim = rPHAnims2,
    _onHit = \p player -> modState player (hardHState  Hurt) >> modLife player (subtract 1),
    _rMove = \player boss dt proj  -> do
      modState proj (modAngle (+ (500*asSeconds dt)))
      spd <- getSpeed proj
      vec <- readIORef $ proj^.rVec
      let norm = sqrt $ (vec^._x)**2 + (vec^._y)**2
      let unary = fmap (/norm) vec
      modPos proj (+ ( fmap (*(spd*asSeconds dt)) unary))
  }
  q <- newQueue

 
  let nextPhase = Death


  phase <- newIORef $ Transition phase1

  return $ Boss life apos stat rWAnims rHAnims speed [pInfo0,pInfo1,pInfo2] q phase




moveAI time player boss = moveU1 (1) 0 time player boss
moveAI2 time player boss = moveU2 (1) 0 1 time player boss
moveAI3 time player boss = moveU3 (1) 0 4 time player boss

p1Keys = [KeyQ,KeyE,KeyR,KeyT,KeyY,KeyU,KeyI,KeyO,KeyP]
p2Keys = p1Keys ++ [KeyF,KeyG,KeyH,KeyJ,KeyK,KeyL]
p3Keys = p2Keys ++ [KeyZ,KeyX,KeyC,KeyV,KeyB,KeyN,KeyM]
p4Keys = [KeyLControl,KeyRAlt,KeyDelete,KeyTab]
phase3 = Phase 400 p3Keys (BAI moveAI3) $ phase1 & phealth .~ 200 & nextPh .~ Death & scramKeys .~ p4Keys
phase2 = Phase 600 p2Keys (BAI moveAI2) phase3
phase1 = Phase 800 p1Keys (BAI moveAI) phase2


initPlayer gEnv = do
  okTex <- err $ textureFromFile (imgPath ++ "ship.png") Nothing
  huTex <- err $ textureFromFile (imgPath ++ "damagedship.png") Nothing
  okAnim <- initAnim' okTex (replicate 4 10)
  huAnim <- initAnim' huTex (replicate 4 10)
  let hanims = [(Okay,okAnim),(Hurt,huAnim)]

  chTex <- err $ textureFromFile (imgPath ++ "charging2.png") Nothing
  idTex <- err $ textureFromFile (imgPath ++ "idle.png") Nothing
  chAnim <- initAnim' chTex (replicate 4 2)
  idAnim <- initAnim' idTex (replicate 4 10)
  let wanims = [(Idle,idAnim),(Charging,chAnim)]

  prTex <- err $ textureFromFile (imgPath ++ "shipProj.png") Nothing
  exTex <- err $ textureFromFile (imgPath ++ "shipProjOverlay.png") Nothing
  prAnim <- initSizedAnim prTex (replicate 4 2) [] (V2 46 46) c0
  exAnim <- initSizedAnim exTex (replicate 4 2) [] (V2 46 46) c0
  let phanims = [(Okay,prAnim),(Hurt,exAnim)]
  mkPlayer gEnv hanims wanims phanims (V2 25 12)

initBoss gEnv = do
  okTex <- err $ textureFromFile (imgPath ++ "boss1.png") Nothing
  huTex <- err $ textureFromFile (imgPath ++ "hurtboss1.png") Nothing
  okAnim <- initSizedAnim okTex (replicate 4 5) [] (V2 46 46) c0
  huAnim <- initSizedAnim huTex (replicate 4 5) [] (V2 46 46) c0
  prTex <- err $ textureFromFile (imgPath ++ "proj2.png") Nothing
  exTex <- err $ textureFromFile (imgPath ++ "proj2ex.png") Nothing
  prAnim <- initSizedAnim prTex (replicate 4 2) [] (V2 23 23) c0
  exAnim <- initSizedAnim exTex (replicate 4 2) [] (V2 23 23) c0
  prTex1 <- err $ textureFromFile (imgPath ++ "proj3.png") Nothing
  exTex1 <- err $ textureFromFile (imgPath ++ "proj3ex.png") Nothing
  prAnim1 <- initSizedAnim prTex1 (replicate 4 2) [] (V2 23 23) c0
  exAnim1 <- initSizedAnim exTex1 (replicate 4 2) [] (V2 23 23) c0
  prTex2 <- err $ textureFromFile (imgPath ++ "proj4.png") Nothing
  exTex2 <- err $ textureFromFile (imgPath ++ "proj4ex.png") Nothing
  prAnim2 <- initSizedAnim prTex2 (replicate 4 2) [] (V2 6 6) c0
  exAnim2 <- initSizedAnim exTex2 (replicate 4 2) [] (V2 12 12) c0
  let hanims = [(Okay,okAnim),(Hurt,huAnim)]
  let phanims = [[(Okay,prAnim),(Hurt,exAnim)],[(Okay,prAnim1),(Hurt,exAnim1)],[(Okay,prAnim2),(Hurt,exAnim2)]]
  mkBoss gEnv hanims [] phanims (V2 15 15)

initBattle :: GameS MenuS -> IO  (GameS BattleS) 
initBattle gS@(InMenu _ w gEnv s) = do

  
  
  v <- getView w
  zoomView v 0.5
  setView w v
  t <- newIORef timeZero

  createTxt (gEnv^.gameText) 8 "^" MenuFont (Just 30) (Just red)
  createTxt (gEnv^.gameText) 4 "<" MenuFont  (Just 30)(Just red)
  createTxt (gEnv^.gameText) 6 ">" MenuFont  (Just 30)(Just red)
  createTxt (gEnv^.gameText) 2 "V" MenuFont  (Just 30)(Just red)
  
  life <- err $ textureFromFile (imgPath ++ "life.png") Nothing
  lanim<-  initAnim' life [30,2,2,5]
  lid <- addAnim' gEnv lanim
  
  blife <- err $ textureFromFile (imgPath ++ "bosslife.png") Nothing
  blanim<-  initAnim' blife [30,2,2,5]
  blid <- addAnim' gEnv blanim

  ship <- initPlayer gEnv
  boss <- initBoss gEnv
  new1 <- HIO.new
  new2 <- HIO.new
  let bS = BattleS t ship boss new1 new2
  

  s <- getSound gEnv 7
  s >?> play
  s' <- getSound gEnv 6
  s' >?> stop
  s' <- getSound gEnv 8
  s' >?> stop
  return $ InBattle Nothing w gEnv bS 




  
-- spawnBossProj ::  GameS -> BattleS -> IO ()
-- spawnBossProj bS@(BattleS time player boss _ _ ) = do
--   proTex <- err $ textureFromFile (imgPath ++ "proj1.png") Nothing
--   proAnim <- initSizedAnim okTex (replicate 4 5) [] (V2 46 46) c0
--   let hanims = [(Okay,proAnim)]
--   bp <- getPos boss
--   let move bS'@(BattleS _ p _ _ _ ) proj dt= do
--     return ()
--   let onHit player = do
--     modLife player (subtract 1)
  
